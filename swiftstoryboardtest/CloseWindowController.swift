//
//  CloseWindowController.swift
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 26.07.21.
//

import SwiftUI

struct CloseWindowController: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct CloseWindowController_Previews: PreviewProvider {
    static var previews: some View {
        CloseWindowController()
    }
}
