//
//  ViewController.swift
//  IWS-Pro Companion
//
//  this is the default viewController. Since this page is only responsible for the quit
//  window which has no logic behind it, this file needs to exits, but has no actual code attached
//
//  Created by Alexandra Klefenz on 24.07.21.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {
        }
    }
}

