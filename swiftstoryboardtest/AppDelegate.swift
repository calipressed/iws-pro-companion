//
//  AppDelegate.swift
//  IWS-Pro Companion App
//
//  This is the main app delegate for the companion app
//  The popover and the menu/status item is created here
//
//  Created by Alexandra Klefenz on 24.07.21.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    // vars for the popover and the status item
    var popover: NSPopover!
    var statusBarItem: NSStatusItem!
    
    // consts for general config
    let stopServerRoute = "http://localhost:5055/F4E6618B7AA2"
    
    
    
    /**
     main entrypoint of the application -> sets up the status item and the popover
     */
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
       
     
        // create the storyboard and the viewcontroller
        let storyboard = NSStoryboard(name: "Dashboard", bundle: nil)
        let vc = storyboard.instantiateInitialController() as! PopoverViewController
        
        // Create the popover
        let popover = NSPopover()
        popover.contentSize = NSSize(width: 300, height: 120)
        popover.behavior = .transient
        popover.contentViewController = vc
        self.popover = popover
        
        // Create the status item
        self.statusBarItem = NSStatusBar.system.statusItem(withLength: 28.0)
        
        // create the button
        if let button = self.statusBarItem.button {
            button.image = #imageLiteral(resourceName: "StatusBarIcon")
            button.image?.size = NSSize(width: 18.0, height: 18.0)
            button.image?.isTemplate = true
            button.action = #selector(togglePopover(_:))
            
            // toggle popover -> triggers the viewDidLoad function of the popover -> webserver is started
            togglePopover(button);
        }
        
        let workspace = NSWorkspace.shared;
        
        workspace.notificationCenter.addObserver(self, selector: #selector(AppDelegate.workspaceWillPowerOff), name: NSWorkspace.willPowerOffNotification, object: workspace); 
    }
    
    /**
     linked handler function for the main menu quit item (when cmd +q is pressed)
     */
    @IBAction func MainMenuQuitApp(_ sender: Any) {
        // stop the server
        stopServer()
        
        // prepare the close info window
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let window = storyboard.instantiateController(withIdentifier: "CloseInfoController") as! CloseInfoController
        
        // show window -> window has a 3.4 sec timer so it should close right before the dispatcher (next line) runs out
        window.showWindow(self)
        
        // create a dispatch timer to wait for a stop and exit the app afterwards
        let seconds = 3.5
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            exit(0)
        }
    }
    
    @objc private func workspaceWillPowerOff(notification: NSNotification) {
        // stop the server
        stopServer()
        
        // prepare the close info window
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let window = storyboard.instantiateController(withIdentifier: "CloseInfoController") as! CloseInfoController
        
        // show window -> window has a 3.4 sec timer so it should close right before the dispatcher (next line) runs out
        window.showWindow(self)
        
        //sleep thread before exiting
        // this must be a "sleep" command since a dispatch queue would just terminate and a error message would be displayed
        sleep(4)
        exit(0)
    }

    /**
     toggles the popover with the button in the menu bar
     */
    @objc func togglePopover(_ sender: AnyObject?) {
        // get button and check if the popover is already displayed or not
        if let button = self.statusBarItem.button {
            if self.popover.isShown {
                self.popover.performClose(sender)
            } else {
                self.popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
            }
        }
    }
    
    /**
        function to stop the server with the provided route
     */
    func stopServer() {
        let url = URL(string: stopServerRoute)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            // handle the result here.
        }.resume()
    }
}

