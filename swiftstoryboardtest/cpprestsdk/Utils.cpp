#pragma warning(disable : 4996)
//
//  Utils.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "Utils.hpp"

#ifdef _WIN32
#include <iomanip>
#include <direct.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#define stat _stat

#elif __APPLE__
#include <unistd.h>
#endif

#include <thread>

using namespace std;

/**
 generates a random alphanumeric string with a given length

 @param len length of the string
 @returns a randomly generated string
 */
string Utils::GenRandom(const int len)
{
	string tmp_s;
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	srand((unsigned)time(NULL) * getpid());

	tmp_s.reserve(len);

	for (int i = 0; i < len; ++i)
		tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];


	return tmp_s;
};
