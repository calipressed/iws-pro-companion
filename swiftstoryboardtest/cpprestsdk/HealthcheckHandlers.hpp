//
//  HealthcheckHandlers.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef HealthcheckHandlers_hpp
#define HealthcheckHandlers_hpp

#include <stdio.h>
#include <cpprest/http_listener.h>

using namespace web::http;

struct HealthcheckHandlers
{
    static void HandleHealthcheck(http_request request);
    static void HandleAppHealthcheck(http_request request);
};

#endif /* HealthcheckHandlers_hpp */
