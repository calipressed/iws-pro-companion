//
//  Utils.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef Utils_hpp
#define Utils_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

struct Utils
{
    static string GenRandom(int len);
};

#endif /* Utils_hpp */
