//
//  WebserverWrapper.h
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 24.07.21.
//

#ifndef WebserverWrapper_h
#define WebserverWrapper_h
#import <Foundation/Foundation.h>

@interface WebserverWrapper : NSObject
- (NSString *) startServer: (bool) debugFlag secondFlag: (bool) nFlag thirdFlag: (bool) oFlag fourthFlag: (bool) pFlag fifthFlag: (bool) lFlag writeToFileFlag: (bool) writeFile;
@end


#endif /* WebserverWrapper_h */
