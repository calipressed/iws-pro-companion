//
//  FileOperations.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef FileOperations_hpp
#define FileOperations_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

struct FileOperations
{
#ifdef __APPLE__
    static int lsof(string filename);
#endif
    static int mkdir(string directory);
    
    static string GetLastChangedDate(string filepath);

    static void remove(string path);
    
    static void SetLsofNFlag(bool flag);
    static void SetLsofOFlag(bool flag);
    static void SetLsofPFlag(bool flag);
    static void SetLsofLFlag(bool flag);    
};

#endif /* FileOperations_hpp */
