//
//  optionHandlers.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "optionHandlers.hpp"
#include <cpprest/http_listener.h>

using namespace web::http;

void
OptionHandlers::HandleGetRoutesOptions(http_request request)
{
    http_response sp(status_codes::NoContent);

#ifdef __APPLE__
    sp.headers().add("Access-Control-Allow-Origin", "*");
    sp.headers().add("Access-Control-Allow-Methods", U("GET, OPTIONS"));
    sp.headers().add("Access-Control-Allow-Headers", "*");
#else
    const utility::string_t field_name1 = U("Access-Control-Allow-Origin");
    const utility::string_t field_name2 = U("Access-Control-Allow-Methods");
    const utility::string_t field_name3 = U("Access-Control-Allow-Headers");
    const utility::string_t value1 = U("*");
    const utility::string_t value2 = U("GET, OPTIONS");
    const utility::string_t value3 = U("*");

    sp.headers()[field_name1] = value1;
    sp.headers()[field_name2] = value2;
    sp.headers()[field_name3] = value3;
#endif
    request.reply(sp);
}

void
OptionHandlers::HandlePostRoutesOptions(http_request request)
{
    http_response sp(status_codes::NoContent);

#ifdef __APPLE__
    sp.headers().add("Access-Control-Allow-Origin", "*");
    sp.headers().add("Access-Control-Allow-Methods", U("POST, OPTIONS"));
    sp.headers().add("Access-Control-Allow-Headers", "*");
#else
    const utility::string_t field_name1 = U("Access-Control-Allow-Origin");
    const utility::string_t field_name2 = U("Access-Control-Allow-Methods");
    const utility::string_t field_name3 = U("Access-Control-Allow-Headers");
    const utility::string_t value1 = U("*");
    const utility::string_t value2 = U("POST, OPTIONS");
    const utility::string_t value3 = U("*");

    sp.headers()[field_name1] = value1;
    sp.headers()[field_name2] = value2;
    sp.headers()[field_name3] = value3;
#endif
    request.reply(sp);
}
