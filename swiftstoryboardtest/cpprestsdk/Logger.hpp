//
//  Logger.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef Logger_hpp
#define Logger_hpp

#include <stdio.h>
#include <iostream>
#include <chrono>

using namespace std;

struct Logger
{
    static void LogLine(string msg);
    static void TRACE(string msg);
    static string GetAllLoggedLines();
    static void WriteLogFile();
};

#endif /* Logger_hpp */
