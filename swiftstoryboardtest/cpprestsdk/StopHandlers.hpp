//
//  StopHandlers.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef StopHandlers_hpp
#define StopHandlers_hpp

#include <stdio.h>

#include <stdio.h>
#include <cpprest/http_listener.h>

using namespace web::http;

struct StopHandlers
{
    static void HandleStop(http_request request);
};

#endif /* StopHandlers_hpp */
