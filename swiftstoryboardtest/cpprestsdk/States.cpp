//
//  States.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "States.hpp"

bool writeFile = false;
bool running = false;
bool debug = false;

bool
States::GetDebug()
{
    return debug;
};

void
States::SetDebug(bool debugFlag) {
    debug = debugFlag;
}

bool
States::GetWriteFile()
{
    return writeFile;
};

void
States::SetWriteFile(bool write)
{
    writeFile = write;
};

bool
States::GetRunning()
{
    return running;
};

void
States::SetRunning(bool isRunning) {
    running = isRunning;
}
