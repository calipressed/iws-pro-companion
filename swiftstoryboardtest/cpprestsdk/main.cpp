#pragma warning(disable : 4996)

#ifdef _WIN32
#include <Windows.h>
#include <iomanip>
#include <direct.h>
#include <shlobj.h>
#include <string>
#elif __APPLE__
#include <thread>
#include <unistd.h>
#endif

#include "States.hpp"
#include "optionHandlers.hpp"
#include "HealthcheckHandlers.hpp"
#include "StopHandlers.hpp"
#include "Logger.hpp"
#include "FileOperations.hpp"
#include "Utils.hpp"
#include "main.h"

#include <cpprest/http_listener.h>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <cpprest/astreambuf.h>
#include <cpprest/filestream.h>
#include <cpprest/streams.h>

#include <codecvt>
#include <iostream>
#include <streambuf>
#include <filesystem>
#include <chrono>

#include <string.h>
#include <cstdint>
#include "nlohmann/json.hpp"

using namespace std;
using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

string LastChangedDate = "";
string saveFolder = "";
string filename = "";
bool casablancaDoSslCertValidation = true;


void setFilename(string name) {
	filename = name;
}

string getFilename() {
	return filename;
}

static int OctalToDecimal(string octal) {
	int octLength = octal.length();
	double dec = 0;

	for (int i = 0; i < octLength; ++i)
	{
		dec += (octal[i] - 48) * pow(8, ((octLength - i) - 1));
	}

	return (int)dec;
}

static string OctalToASCII(string oct) {
	string ascii = "";
	int octLen = oct.length();

	for (int i = 0; i < octLen; i += 3)
	{
		ascii += (char)OctalToDecimal(oct.substr(i, 3));
	}

	return ascii;
}



/**
 Downloads a file from a url to the local disc

 @param downloadUrl URL for the GET request
 @param saveToFilePath destination path for the file on the local disc
 */
string DownloadFile(string downloadUrl, string saveToFilePath, string token) {

	string result = "";
	bool errorOccured = false;

	std::vector<std::pair<std::ifstream, std::string>> pass_data = {};

	if (States::GetDebug()) {
		Logger::TRACE("Download file from: '" + downloadUrl + "'\n and save to: " + saveToFilePath);
	}

	// create config for certificate validation
	web::http::client::http_client_config config;
	config.set_validate_certificates(false);

#ifdef __APPLE__
		web::http::client::http_client api(U(downloadUrl));

	// check if cert validation is turned on or off
	if (!casablancaDoSslCertValidation) {
		web::http::client::http_client api(U(downloadUrl), config);
	}

	// create client to GET file
	web::http::http_request msg{ methods::GET };

	// add token
	msg.headers().add("Authorization", token);
#else
	web::http::client::http_client api(converter.from_bytes(downloadUrl));

	// check if cert validation is turned off -> overwrite the var
	if (!casablancaDoSslCertValidation) {
		web::http::client::http_client api(converter.from_bytes(downloadUrl), config);
	}

	// create client to GET file
	web::http::http_request msg{ methods::GET };

	// add token
	const utility::string_t authorization = U("Authorization");
	msg.headers()[authorization] = converter.from_bytes(token);
#endif

	try {
		// make GET request to download file
		api.request(msg)
			.then([=](http_response response) {

			utility::string_t contentDisp = response.headers()[U("Content-Disposition")];

#ifdef __APPLE__

			string con = contentDisp;

			std::size_t found = con.find("\"");
			if (found != std::string::npos) {
				con = con.substr(found + 1);
			}

			std::size_t found2 = con.find("\"");
			if (found2 != std::string::npos) {
				con = con.substr(0, found2);
			}

			setFilename(con);

			Logger::LogLine("Filename: " + string(con));
#else
			string con = converter.to_bytes(contentDisp);


			std::size_t found = con.find("\"");
			if (found != std::string::npos) {
				con = con.substr(found + 1);
			}

			std::size_t found2 = con.find("\"");
			if (found2 != std::string::npos) {
				con = con.substr(0, found2);
			}

			setFilename(con);

			Logger::LogLine("Filename: " + con);
#endif

			return response.body();
				}).then([=](Concurrency::streams::basic_istream<uint8_t> is) {

					// save file to local disc
					string filepath = saveToFilePath + getFilename();
#ifdef __APPLE__
					auto fileStream = concurrency::streams::fstream::open_ostream(utility::conversions::latin1_to_utf8(filepath), std::ios::out | std::ios::binary).get();

					// Write input from request body directly to output filestream
					is.read_to_end(fileStream.streambuf()).wait();
					fileStream.close().wait();
#else
					Concurrency::streams::streambuf<uint8_t> rwbuf = Concurrency::streams::file_buffer<uint8_t>::open(converter.from_bytes(filepath)).get();

					is.read_to_end(rwbuf).get();
					rwbuf.close();
#endif
					// get last changed Date
					LastChangedDate = FileOperations::GetLastChangedDate(saveToFilePath);
					})
					.wait();
	}
	catch (web::http::http_exception e)
	{
		string errorwhat = e.what();
		Logger::LogLine("Error while downloading the File: " + errorwhat);
		Logger::LogLine("Maybe try to disable certificate validation");
		errorOccured = true;
	}
	catch (...)
	{
		wcout << "Unknown Error while downloading the file";
		errorOccured = true;
	}

	if (States::GetDebug() && !errorOccured) {
		Logger::TRACE("Download finished");
	}

	if (!errorOccured) {
		return saveToFilePath + getFilename();
	}
	else {
		return "";
	}

}

/**
 reads a file as streambuffer and returns the bytes as string

 @param ifs ifstream of the file to read
 @returns file as string
 */
string read_entire_file(std::ifstream& ifs) {
	return std::string((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
}

/**
 generates the form data for the file post

 @param strm vector to include into the data
 @returns pair of stringscontaining the data and the boundary
 */
std::pair<std::string, std::string> generate_form_data(std::vector<std::pair<std::ifstream, std::string>>& strm) {
	std::stringstream data;

	std::string boundary{};
	for (int i = 0; i < 50; i++) {
		boundary += (rand() % 26) + 'A';
	}

	for (auto& each : strm) {
		std::string filename = "file";
		data << "--" << boundary << "\r\n";
		data << "Content-Disposition: form-data; name=\"file\"; filename=\""
			<< each.second << "\"\r\nContent-Type: application/octet-stream\r\n\r\n"
			<< read_entire_file(each.first) << "\r\n\r\n";
	}
	data << "--" << boundary << "--";

	return { boundary, data.str() };
}

/**
 sends the local file back to the server

 @param filename name and path of the file that should be sent to the server
 @param token one time use token to use for the upload
 @param sendToUrl url to the worker-process
 */
void SendFile(string filename, string token, string sendToUrl) {

	// check if last changed date has changed
	if (FileOperations::GetLastChangedDate(filename) == LastChangedDate) {
		Logger::TRACE("File was not changed");
	}

	std::vector<std::pair<std::ifstream, std::string>> pass_data = {};

#ifdef __APPLE__
	pass_data.emplace_back(std::ifstream{ utility::conversions::latin1_to_utf8(filename), std::ios::binary }, utility::conversions::latin1_to_utf8(filename));
#else
	pass_data.emplace_back(std::ifstream{ filename, std::ios::binary }, filename);
#endif

	if (States::GetDebug()) {
		Logger::TRACE("Generating form data");
	}

	auto p = generate_form_data(pass_data);

#ifdef __APPLE__
	web::http::client::http_client client(sendToUrl);
#else
	web::http::client::http_client client(converter.from_bytes(sendToUrl));
#endif

	web::http::http_request msg{ methods::POST };

	if (States::GetDebug()) {
		Logger::TRACE("Generate request");
	}

	msg.set_body(p.second, "multipart/form-data; boundary=" + p.first);

#ifdef __APPLE__
	msg.headers().add("Authorization", token);
	msg.headers().add("Accept", "*/*");
#else
	// add token
	const utility::string_t authorization = U("Authorization");
	const utility::string_t accept = U("Accept");
	const utility::string_t acceptValue = U("*/*");

	msg.headers()[authorization] = converter.from_bytes(token);
	msg.headers()[accept] = acceptValue;
#endif

	try {
		if (States::GetDebug()) {
			Logger::TRACE("Sending file");
		}

		auto req = client.request(msg);

		req.wait();

		req.then([](http_response resp_val) {
			Logger::TRACE("File sent back to IWS-Pro");

            
            auto response = resp_val.extract_utf8string(true).get();
            try {
                auto parsed_json = response.empty() ? nlohmann::json{} : nlohmann::json::parse(response);
                
                Logger::TRACE("Response: " + parsed_json.dump());
            } catch(...) {
                Logger::TRACE("Response: " + response);
            }
			

			});
	}
	catch (exception const& e)
	{
		Logger::TRACE(e.what());
	}

	// removing file
	FileOperations::remove(filename);
}

/**
 Handles the file lock for the downloaded file and uploads the file to the server after that

 @param filename name and path of the file that is observed
 @param token token to use later in the reupload of the file
 @param sendToUrl url for the fileupload after the lock
 */
void HandleFileLock(string filename, string token, string sendToUrl)
{
	int sleepUntilCheck = 5000;
	int sleepBetweenChecks = 4000;

	if (States::GetDebug()) {
		Logger::TRACE("Opening downloaded file in default application");
	}

	// default file open (different for osx and win) and wait a specific time until fileopen is checked
#ifdef _WIN32
	ShellExecute(NULL, NULL, converter.from_bytes(filename).c_str(), NULL, NULL, SW_SHOWNORMAL);
	Sleep(sleepUntilCheck);
#elif __APPLE__
	string command = "open " + utility::conversions::latin1_to_utf8(filename);
	system(command.c_str());
	std::this_thread::sleep_for(std::chrono::milliseconds(sleepUntilCheck));
#endif

	// only check if uploadurl was supplies -> otherwise readonly open
	if (sendToUrl != "") {

		if (States::GetDebug()) {
			Logger::TRACE("Check every " + to_string(sleepBetweenChecks) + "ms if file is still locked");
		}

		// check if file is opened for edit (is locked by another program)
#ifdef _WIN32
		ofstream myfile;
		myfile.open(converter.from_bytes(filename), ios_base::app);
		while (!myfile.is_open()) {

			if (States::GetDebug()) {
				Logger::TRACE("File is still open");
			}

			Sleep(sleepBetweenChecks);
			myfile.open(converter.from_bytes(filename), ios_base::app);
		}
		myfile.close();
#elif __APPLE__
		bool loop = true;
		while (loop) {
			if (FileOperations::lsof(utility::conversions::latin1_to_utf8(filename)) == 0) {

				if (States::GetDebug()) {
					Logger::TRACE("File is still open");
				}

				std::this_thread::sleep_for(std::chrono::milliseconds(sleepBetweenChecks));
			}
			else {
				loop = false;
			}
	}
#endif
		if (States::GetDebug()) {
			Logger::TRACE("File was closed");
		}

		// file is now unlocked -> send file back to server
		SendFile(filename, token, sendToUrl);
}
	else {
		Logger::TRACE("File is readonly");
	}
}

void HandleRequest(
	http_request request,
	function<void(json::value const&, json::value&)> action)
{
	auto answer = json::value::object();

	request
		.extract_json()
		.then([&answer, &action](pplx::task<json::value> task) {
		try
		{
			auto const& jvalue = task.get();

			if (!jvalue.is_null())
			{
				action(jvalue, answer);
			}
		}
		catch (http_exception const& e)
		{
			wcout << e.what() << endl;
		}
			})
		.wait();

			http_response response(status_codes::OK);

#ifdef __APPLE__
			response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
#elif _WIN32
			const utility::string_t acao = U("Access-Control-Allow-Origin");
			const utility::string_t acaoValue = U("*");

			response.headers()[acao] = acaoValue;
#endif // __APPLE__


			response.set_body(answer);
			request.reply(response);
}

void HandleNewFilePost(http_request request)
{
	// vars for the payload values
	string token = "";
	string downloadUrl = "";
	string uploadUrl = "";

	// var for possible error text
	string errorText = "";

	// var for json answer
	auto answer = json::value::object();

	// get request json
	const json::value& v = request.extract_json().get();

	// create new filepath
#ifdef __APPLE__
	string tmpdir = getenv("TMPDIR");
#else
	char* pValue;
	size_t len;
	errno_t err = _dupenv_s(&pValue, &len, "TEMP");

	string tmpdir = string(pValue) + "\\IWS-Pro-Companion\\";
#endif

	// generate filepath
	string savefilePath = "";

	if (saveFolder == "") {
		savefilePath = tmpdir + Utils::GenRandom(20);
	}
	else {
		savefilePath = saveFolder + "\\" + Utils::GenRandom(20);
	}
	FileOperations::mkdir(savefilePath);

	Logger::TRACE("New POST to '/newFile'");

	// add folder seperator
#ifdef __APPLE__
	savefilePath += "/";
#else
	savefilePath += "\\";
#endif 

	// try to get all values
	try {
#ifdef __APPLE__
		token = v.at(U("token")).as_string();
#else
		token = converter.to_bytes(v.at(L"token").as_string());
#endif
}
	catch (exception const& e) {
		errorText += "'token' ";
		errorText += e.what();
	}

	try {
#ifdef __APPLE__
		downloadUrl = v.at(U("downloadUrl")).as_string();
#else
		downloadUrl = converter.to_bytes(v.at(L"downloadUrl").as_string());
#endif
	}
	catch (exception const& e) {
		if (errorText != "") {
			errorText += " ";
		}

		errorText += "'downloadUrl' ";
		errorText += e.what();
	}

	try {
#ifdef __APPLE__
		uploadUrl = v.at(U("uploadUrl")).as_string();
#else
		uploadUrl = converter.to_bytes(v.at(L"uploadUrl").as_string());
#endif
	}
	catch (exception const& e) {
		// do nothing
	}

	// check for errors
	if (errorText != "") {

		http_response response(status_codes::BadRequest);
#ifdef __APPLE__
		answer["code"] = json::value::number(status_codes::BadRequest);
		answer["msg"] = json::value::string(errorText);
		response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
#else
		answer[L"code"] = json::value::number(status_codes::BadRequest);
		answer[L"msg"] = json::value::string(converter.from_bytes(errorText));

		const utility::string_t acao = U("Access-Control-Allow-Origin");
		const utility::string_t acaoValue = U("*");

		response.headers()[acao] = acaoValue;
#endif
		response.set_body(answer);
		request.reply(response);
	}
	else {
		http_response response(status_codes::OK);
#ifdef __APPLE__
		answer["code"] = json::value::number(status_codes::OK);
		answer["msg"] = json::value::string("request received");
		response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
#else
		answer[L"code"] = json::value::number(status_codes::OK);
		answer[L"msg"] = json::value::string(L"request received");

		const utility::string_t acao = U("Access-Control-Allow-Origin");
		const utility::string_t acaoValue = U("*");

		response.headers()[acao] = acaoValue;
#endif
		response.set_body(answer);
		request.reply(response);

		// download file
		const string savedFilePath = DownloadFile(downloadUrl, savefilePath, token);

		HandleFileLock(savedFilePath, token, uploadUrl);
	}
}

void Webserver::startServer(bool debugFlag, bool nFlag, bool oFlag, bool pFlag, bool lFlag, bool writeFile, bool doCertificateValidation)
{
	States::SetRunning(true);
	States::SetWriteFile(writeFile);

	Logger::LogLine("IWS-Pro-companion-app");
	Logger::LogLine("***********************");
	Logger::LogLine("");

	// check if debug-flag was supplied
	if (debugFlag) {
		States::SetDebug(true);
		Logger::LogLine("Debug Flag set");
	}

	if (nFlag) {
		FileOperations::SetLsofNFlag(true);

		if (States::GetDebug()) {
			Logger::LogLine("Flag '-n' set for lsof");
		}
	}

	if (oFlag) {
		FileOperations::SetLsofOFlag(true);

		if (States::GetDebug()) {
			Logger::LogLine("Flag '-O' set for lsof");
		}
	}

	if (pFlag) {
		FileOperations::SetLsofPFlag(true);

		if (States::GetDebug()) {
			Logger::LogLine("Flag '-P' set for lsof");
		}
	}

	if (lFlag) {
		FileOperations::SetLsofLFlag(true);

		if (States::GetDebug()) {
			Logger::LogLine("Flag '-l' set for lsof");
		}
	}

	if (!doCertificateValidation) {
		casablancaDoSslCertValidation = false;
		Logger::LogLine("!!! SSL CERTIFICATE VALIDATION IS TURNED OFF !!!");
	}

	if (States::GetDebug()) {
		Logger::LogLine("");
	}

	// create a listener for the healthcheck and the newFile route
#ifdef __APPLE__
	http_listener listener("http://0.0.0.0:5055/newFile");
	http_listener healthListener("http://0.0.0.0:5055/healthcheck");
	http_listener appHealthListener("http://0.0.0.0:5055/1DF1AB64");
	http_listener stopListener("http://0.0.0.0:5055/F4E6618B7AA2"); // stop route -> alphanumeric name is used so it is not used unintentionally
#else
	http_listener healthListener(L"http://127.0.0.1:5055/healthcheck");
	http_listener listener(L"http://127.0.0.1:5055/newFile");
	http_listener appHealthListener(L"http://127.0.0.1:5055/1DF1AB64");
	http_listener stopListener(L"http://127.0.0.1:5055/F4E6618B7AA2"); // stop route -> alphanumeric name is used so it is not used unintentionally
#endif

	// add options support
	healthListener.support(methods::OPTIONS, OptionHandlers::HandleGetRoutesOptions);
	appHealthListener.support(methods::OPTIONS, OptionHandlers::HandleGetRoutesOptions);
	listener.support(methods::OPTIONS, OptionHandlers::HandlePostRoutesOptions);
	stopListener.support(methods::OPTIONS, OptionHandlers::HandleGetRoutesOptions);

	// add method support and handlers
	healthListener.support(methods::GET, HealthcheckHandlers::HandleHealthcheck);
	appHealthListener.support(methods::GET, HealthcheckHandlers::HandleAppHealthcheck);
	listener.support(methods::POST, HandleNewFilePost);
	stopListener.support(methods::GET, StopHandlers::HandleStop);

	try
	{
		// start listeners
		healthListener
			.open()
			.then([&healthListener]()
				{
					Logger::LogLine("starting to listen for healthcheck");
				})
			.wait();

				listener
					.open()
					.then([&listener]()
						{
							Logger::LogLine("starting to listen for new files");
						})
					.wait();

						stopListener
							.open()
							.then([&stopListener]()
								{
									Logger::LogLine("starting to listen for stop");
								})
							.wait();

								appHealthListener
									.open()
									.then([&appHealthListener]()
										{
											Logger::LogLine("starting to listen for app healthchecks");
										})
									.wait();

										// keep program running
										while (States::GetRunning()) {
											//sleep thread -> decrease CPU usage
											this_thread::sleep_for(chrono::milliseconds(2000));
										}

										Logger::TRACE("Closing Listener");
										//stop program
										healthListener.close().wait();
										listener.close().wait();
										stopListener.close().wait();
										appHealthListener.close().wait();

										Logger::TRACE("Listener closed");
	}
	catch (exception const& e)
	{
		wcout << e.what() << endl;
	}

	Logger::TRACE("Done");

	if (States::GetWriteFile()) {
		Logger::WriteLogFile();
	}

	return;
}

#ifdef _WIN32
/**
 Main method if the code is used as a cli

 @param argc argument count
 @param argv all arguments (including binary name) used to start the cli
 @returns integer return code
 */
int main(int argc, char* argv[]) {
	// unwrap the arguments
	bool debugArg = false;
	bool nArg = false;
	bool oArg = false;
	bool pArg = false;
	bool lArg = false;
	bool writeOutputFile = true;
	bool doCertificateValidation = true;

	if (argc >= 1) {
		for (int i = 0; i < argc; i++) {
			string arg = argv[i];

			if (arg.substr(0, 1) == "-") {
				if (arg == "-d") {
					debugArg = true;
				}
				else if (arg == "-n") {
					nArg = true;
				}
				else if (arg == "-O") {
					oArg = true;
				}
				else if (arg == "-P") {
					pArg = true;
				}
				else if (arg == "-l") {
					lArg = true;
				}
				else if (arg == "--no-output") {
					writeOutputFile = false;
				}
				else if (arg.substr(0, 9) == "--folder=") {
					saveFolder = arg.substr(9);
				}
				else if (arg == "--no-certificate-validation") {
					doCertificateValidation = false;
				}
			}
		}
	}

	// start the server
	Webserver::startServer(debugArg, nArg, oArg, pArg, lArg, writeOutputFile, doCertificateValidation);

	return 0;
}
#endif
