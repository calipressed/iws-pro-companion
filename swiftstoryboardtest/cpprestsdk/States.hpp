//
//  States.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef States_hpp
#define States_hpp

#include <stdio.h>

struct States
{
    static bool GetDebug();
    static void SetDebug(bool debugFlag);
    
    static bool GetRunning();
    static void SetRunning(bool isRunning);
    
    static bool GetWriteFile();
    static void SetWriteFile(bool write);
};

#endif /* States_hpp */
