#pragma warning(disable : 4996)
//
//  Logger.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "Logger.hpp"
#include "FileOperations.hpp"
#include "Utils.hpp"
#include <string.h>
#include <codecvt>
#include <cpprest/streams.h>

#ifndef __APPLE__
#include <iomanip>
#endif

using namespace std;

string linesLogged = "";
std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> stringConverter;

string return_current_time_and_date()
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    
    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
    return ss.str();
}

void
Logger::LogLine(string msg) {
    
#ifdef __APPLE__
    cout << msg << "\n";
#else
    wcout << stringConverter.from_bytes(msg) << "\n";
#endif
    linesLogged += msg + "\n";
}

void
Logger::TRACE(string msg) {
    LogLine("{\"timestamp\":\"" + return_current_time_and_date() + "\",\"info\":\"" + msg + "\"}");
}

string
Logger::GetAllLoggedLines() {
    return linesLogged;
}

void
Logger::WriteLogFile() {
    
    // make dir
#ifdef __APPLE__
    string supportDir = string(getenv("HOME")) + "/Library/Application Support/iwsProCompanion";
#else
    char* pValue;
    size_t len;
    errno_t err = _dupenv_s(&pValue, &len, "APPDATA");
    
    string supportDir = string(pValue) + "\\IWS-Pro-Companion";
#endif
    
    FileOperations::mkdir(supportDir);
    ofstream myfile;
    
#ifdef __APPLE__
    myfile.open(supportDir + "/" + Utils::GenRandom(10) + "_iwsprocompanion.log");
#else
    myfile.open(supportDir + "\\" + Utils::GenRandom(10) + "_iwsprocompanion.log");
#endif
    myfile << Logger::GetAllLoggedLines();
    myfile.close();
}

