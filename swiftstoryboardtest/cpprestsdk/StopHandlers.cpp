//
//  StopHandlers.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "StopHandlers.hpp"
#include "Logger.hpp"
#include "States.hpp"

#include <stdio.h>
#include <cpprest/http_listener.h>

using namespace web;
using namespace web::http;


void
StopHandlers::HandleStop(http_request request)
{
    Logger::TRACE("Handle Stop");
    
    auto answer = json::value::object();
    
#ifdef __APPLE__
    answer["code"] = json::value::string("200");
#else
    answer[L"code"] = json::value::string(L"200");
#endif
    
    http_response response(status_codes::OK);
    response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
    response.set_body(answer);
    request.reply(response);
    
    if (States::GetWriteFile()) {
        Logger::WriteLogFile();
    }
    
    States::SetRunning(false);
}
