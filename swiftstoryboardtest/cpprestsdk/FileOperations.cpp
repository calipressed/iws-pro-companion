//
//  FileOperations.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "FileOperations.hpp"
#include "Logger.hpp"
#include "States.hpp"

#ifdef __APPLE__
#include <thread>
#include <unistd.h>
#else
#define stat _stat
#include <Windows.h>
#include <iomanip>
#include <direct.h>
#include <shlobj.h>
#include <string.h>
#include <nlohmann/json.hpp>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

using namespace std;

// lsof flags
bool lsofFlagN = false;
bool lsofFlagL = false;
bool lsofFlagO = false;
bool lsofFlagP = false;

#ifdef __APPLE__

int
FileOperations::lsof(string filename) {
    int pid, status;
    // first we fork the process
    if (pid = fork()) {
        
        if (States::GetDebug()) {
            Logger::TRACE("Forking process");
        }
        
        // pid != 0: this is the parent process (i.e. our process)
        waitpid(pid, &status, 0); // wait for the child to exit
        
        if (States::GetDebug()) {
            Logger::TRACE("Fork closed");
        }
    }
    else {
        /* pid == 0: this is the child process. now let's load the
         "ls" program into this process and run it */
        
        const char executable[] = "/usr/sbin/lsof";
        
        // load it. there are more exec__ functions, try 'man 3 exec'
        // execl takes the arguments as parameters. execv takes them as an array
        // this is execl though, so:
        //      exec         argv[0]  argv[1] end
        
        
        // check if a flag was set -> change execl
        if (lsofFlagN || lsofFlagL || lsofFlagO || lsofFlagP) {
            string flags = "-";
            
            if (lsofFlagN) {
                flags += "n";
            }
            if (lsofFlagL) {
                flags += "l";
            }
            if (lsofFlagO) {
                flags += "O";
            }
            if (lsofFlagP) {
                flags += "P";
            }
            
            if (States::GetDebug()) {
                Logger::TRACE("Executing 'lsof " + flags + " " + filename + "'");
            }
            
            execl(executable, executable, flags.c_str(), filename.c_str(), NULL);
        }
        else {
            
            if (States::GetDebug()) {
                Logger::TRACE("Executing 'lsof " + filename + "'");
            }
            
            execl(executable, executable, filename.c_str(), NULL);
        }
        
        /* exec does not return unless the program couldn't be started.
         when the child process stops, the waitpid() above will return.
         */
        
    }
    return status; // this is the parent process again.
}

int
FileOperations::mkdir(string directory) {
    int pid, status;
    // first we fork the process
    if (pid = fork()) {
        
        if (States::GetDebug()) {
            Logger::TRACE("Forking process");
        }
        
        // pid != 0: this is the parent process (i.e. our process)
        waitpid(pid, &status, 0); // wait for the child to exit
        
        if (States::GetDebug()) {
            Logger::TRACE("Fork closed");
        }
    }
    else {
        /* pid == 0: this is the child process. now let's load the
         "ls" program into this process and run it */
        
        const char executable[] = "/bin/mkdir";
        
        // load it. there are more exec__ functions, try 'man 3 exec'
        // execl takes the arguments as parameters. execv takes them as an array
        // this is execl though, so:
        //      exec         argv[0]  argv[1] end
        
        if (States::GetDebug()) {
            Logger::TRACE("Executing 'mkdir " + directory + "'");
        }
        
        execl(executable, executable, directory.c_str(), NULL);
        
        /* exec does not return unless the program couldn't be started.
         when the child process stops, the waitpid() above will return.
         */
        
    }
    return status; // this is the parent process again.
};
#elif WIN32
int
FileOperations::mkdir(string directory)
{
    _mkdir(directory.c_str());
    return 0;
};
#endif

void 
FileOperations::remove(string path) {

    std::remove(path.c_str());
}

string
FileOperations::GetLastChangedDate(string filepath) {
    
    string res = "";
    struct stat result;
    
    if (stat(filepath.c_str(), &result) == 0)
    {
        long mod_time = result.st_mtime;
        
        res = to_string(mod_time);
    }
    
    return res;
}

void
FileOperations::SetLsofNFlag(bool flag)
{
    lsofFlagN = flag;
}

void
FileOperations::SetLsofOFlag(bool flag)
{
    lsofFlagO = flag;
}

void
FileOperations::SetLsofPFlag(bool flag)
{
    lsofFlagP = flag;
}

void FileOperations::SetLsofLFlag(bool flag)
{
    lsofFlagL = flag;
}
