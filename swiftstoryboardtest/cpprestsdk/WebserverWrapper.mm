//
//  HelloWorldWrapper.m
//  iwsproui
//
//  Created by Alexandra Klefenz on 16.07.21.
//

#import <Foundation/Foundation.h>

#import "WebserverWrapper.h"
#import "main.h"
@implementation WebserverWrapper
- (NSString *) startServer: (bool) debugFlag secondFlag: (bool) nFlag thirdFlag: (bool) oFlag fourthFlag: (bool) pFlag fifthFlag: (bool) lFlag writeToFileFlag:(bool)writeFile {
    Webserver server;
    
    server.startServer(debugFlag, nFlag, oFlag, pFlag, lFlag, writeFile, false);
    std::string msg = "running";
    return [NSString
            stringWithCString:msg.c_str()
            encoding:NSUTF8StringEncoding];
}
@end
