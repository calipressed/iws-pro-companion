//
//  HealthcheckHandlers.cpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#include "Logger.hpp"
#include "HealthcheckHandlers.hpp"

using namespace web;
using namespace web::http;

void
HealthcheckHandlers::HandleHealthcheck(http_request request)
{
    Logger::TRACE("Handle Healthcheck");
    
    auto answer = json::value::object();
    http_response response(status_codes::OK);

#ifdef __APPLE__
    answer["code"] = json::value::string("200");

    response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
#else
    answer[L"code"] = json::value::string(L"200");

    const utility::string_t field_name1 = L"Access-Control-Allow-Origin";
    const utility::string_t value1 = L"*";
    response.headers()[field_name1] = value1;
#endif

    response.set_body(answer);
    request.reply(response);
}

void
HealthcheckHandlers::HandleAppHealthcheck(http_request request)
{
    auto answer = json::value::object();
    http_response response(status_codes::OK);
    
#ifdef __APPLE__
    answer["code"] = json::value::string("200");

    response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
#else
    answer[L"code"] = json::value::string(L"200");

    const utility::string_t field_name1 = L"Access-Control-Allow-Origin";
    const utility::string_t value1 = L"*";
    response.headers()[field_name1] = value1;
#endif
    
    response.set_body(answer);
    request.reply(response);
}
