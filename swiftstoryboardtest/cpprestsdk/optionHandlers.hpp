//
//  optionHandlers.hpp
//  swiftstoryboardtest
//
//  Created by Alexandra Klefenz on 29.07.21.
//

#ifndef optionHandlers_hpp
#define optionHandlers_hpp

#include <stdio.h>
#include <cpprest/http_listener.h>

using namespace web::http;

struct OptionHandlers
{
    static void HandleGetRoutesOptions(http_request request);
    static void HandlePostRoutesOptions(http_request request);
};

#endif /* optionHandlers_hpp */
