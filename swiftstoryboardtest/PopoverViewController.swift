//
//  PopoverViewController.swift
//  IWS-Pro Companion App
//
//  This is the ViewController for the popover UI used for the MenuBarItem
//  The User can start and stop the server manually and see if the server is running.
//  This is also the main entrypoint for the application
//
//  Created by Alexandra Klefenz on 24.07.21.
//

import Cocoa

class PopoverViewController: NSViewController {
    
    // create vars for the lsof flags
    var nOn = false;
    var oOn = false;
    var pOn = false;
    var lOn = false;
    
    // create the links to the UI elements -> Buttons and checkboxes
    @IBOutlet weak var btnQuit: NSButton!
    @IBOutlet weak var btnStop: NSButton!
    @IBOutlet weak var btnStart: NSButton!
    @IBOutlet weak var chbNFlag: NSButton!
    @IBOutlet weak var chbOFlag: NSButton!
    @IBOutlet weak var chbPFlag: NSButton!
    @IBOutlet weak var chbLFlag: NSButton!
    @IBOutlet weak var lblRunning: NSTextField!
    
    // internal indicatior if the server is running
    var serverRunning = false;
    
    // general config infos concerning the webserver and checks
    var internalHealthcheckRoute = "http://localhost:5055/1DF1AB64"
    var stopServerRoute = "http://localhost:5055/F4E6618B7AA2"
    var healthcheckWaitTime = 10.0
    
    /**
     entry function
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        // start the server in the background
        btnStartPress(btnStart)
        
        // display that the server is running
        changeUi(UiStates.running)
    }
    
    /**
     Handler funtion for the stop button
     stops the webserver in the background and changes the ui accordingly
     */
    @IBAction func btnStopPress(_ sender: Any) {
        // change buttons
        changeUi(UiStates.stopping)
        stopServer()
        let seconds = 3.5
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.changeUi(UiStates.stopped)
        }
    }
    
    /**
     handler function for the start button
     starts the server with the provided flags and changes the ui accordingly
     */
    @IBAction func btnStartPress(_ sender: NSButton) {
        
        // set ui to starting
        changeUi(UiStates.starting)
        serverRunning = true
        
        // start server
        startWebserver(debugFlag: true, nFlag: nOn, oFlag: oOn, pFlag: pOn, lFlag: lOn)
        
        // wait for the server to start and change the ui again to display a running state
        let seconds = 1.5
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.changeUi(UiStates.running)
        }
        
        // do healthchecks every few seconds to check if the server is still running
        DispatchQueue.global(qos: .background).async {
            // wait for two extra seconds before the first check -> enough time for the server to start
            Thread.sleep(forTimeInterval: 2)
            
            // check in a loop until the server is not running anymore (wait some time inbetween the checks)
            while(self.serverRunning) {
                // set url to the internal healthcheck route
                let url = URL(string: self.internalHealthcheckRoute)!
                
                // create simple get request
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                
                // make request and check the result in a callback function
                URLSession.shared.dataTask(with: request) { data, response, error in
                    // check if errors occured
                    if error != nil {
                        self.serverRunning = false
                        self.changeUi(UiStates.stopped)
                        return
                    }
                    let response = response as! HTTPURLResponse
                    let status = response.statusCode
                    if (status <= 299 && status >= 200) {
                        // everything okay -> wait for next check
                        return
                    }else {
                        // something wrong -> server stopping
                        self.serverRunning = false
                        self.changeUi(UiStates.stopped)
                        return
                    }
                }.resume();
                
                // wait some time until the next check is started
                Thread.sleep(forTimeInterval: self.healthcheckWaitTime)
            }
        }
    }
    
    /**
     handler function for the quit button
     quits the application entirely -> exits with code 0
     */
    @IBAction func btnQuitPress(_ sender: Any) {
        if (btnQuit.isEnabled) {
            exit(0)
        }
    }
    
    /**
     handler function for the checked of the n checkbox
     */
    @IBAction func chbNChecked(_ sender: NSButton) {
        switch sender.state {
        case .on:
            self.nOn = true;
        case .off:
            self.nOn = false;
        case .mixed:
            self.nOn = false;
        default: break
        }
    }
    
    /**
     handler function for the checked of the o checkbox
     */
    @IBAction func chbOChecked(_ sender: NSButton) {
        switch sender.state {
        case .on:
            self.oOn = true;
        case .off:
            self.oOn = false;
        case .mixed:
            self.oOn = false;
        default: break
        }
    }
    
    /**
     handler function for the checked of the P checkbox
     */
    @IBAction func chbPFlag(_ sender: NSButton) {
        switch sender.state {
        case .on:
            self.pOn = true;
        case .off:
            self.pOn = false;
        case .mixed:
            self.pOn = false;
        default: break
        }
    }
    
    /**
     handler function for the checked of the l checkbox
     */
    @IBAction func chbLChecked(_ sender: NSButton) {
        switch sender.state {
        case .on:
            self.lOn = true;
        case .off:
            self.lOn = false;
        case .mixed:
            self.lOn = false;
        default: break
        }
    }
    
    /**
     starts the webserver in a dispatcher with the provided flags
     */
    func startWebserver(debugFlag:Bool, nFlag:Bool, oFlag:Bool, pFlag:Bool, lFlag:Bool) {
        DispatchQueue.global(qos: .background).async {
            WebserverWrapper().startServer(debugFlag, secondFlag: nFlag, thirdFlag: oFlag, fourthFlag: pFlag, fifthFlag: lFlag, writeToFileFlag: true)
        }
    }
    
    /**
     stops the server using the stop route
     */
    func stopServer() {
        let url = URL(string: stopServerRoute)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            // handle the result here.
        }.resume()
    }
    
    /**
     changes the ui depending on the given state
     */
    func changeUi(_ state:UiStates) {
        // push back to main thread (could be on a different thread and ui updates are only allowed on main)
        DispatchQueue.main.async {
            // buttondisplay is the same for running, starting and stopping
            if (state == UiStates.running || state == UiStates.starting || state == UiStates.stopping) {
                // change buttons
                self.btnQuit.isEnabled = false
                self.btnStart.isEnabled = false
                self.btnStop.isEnabled = true
                
                //disable checkboxes
                self.chbNFlag.isEnabled = false
                self.chbOFlag.isEnabled = false
                self.chbPFlag.isEnabled = false
                self.chbLFlag.isEnabled = false
                
                // only difference is the displayed text
                if (state == UiStates.running) {
                    self.lblRunning.stringValue = "running"
                }
                
                if (state == UiStates.starting) {
                    self.lblRunning.stringValue = "starting"
                }
                
                if (state == UiStates.stopping) {
                    self.lblRunning.stringValue = "stopping"
                }
            } else {
                // change buttons
                self.btnQuit.isEnabled = true
                self.btnStart.isEnabled = true
                self.btnStop.isEnabled = false
                
                // enable checkboxes
                self.chbNFlag.isEnabled = true
                self.chbOFlag.isEnabled = true
                self.chbPFlag.isEnabled = true
                self.chbLFlag.isEnabled = true
                
                // change label text
                self.lblRunning.stringValue = "stopped"
            }
        }
    }
}

enum UiStates {
    case starting
    case stopping
    case running
    case stopped
}
