//
//  CloseInfoController.swift
//  IWS-Pro Companion App
//
//  This WindowController is used for the close Window when quitting the app with cmd + q.
//  This is a part of the overwritten function that displays this window as a userinfo while the webserver is gracefully stopped in the background
//
//  Created by Alexandra Klefenz on 26.07.21.
//

import Cocoa

class CloseInfoController: NSWindowController {
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        // check if the window did load
        guard window != nil else {
            return
        }
                
        // close the window after 3.4 seconds (3.5 seconds is the "wait time" for the dispatcher that stopps the application)
        let seconds = 3.4
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.close()
        }
    }
    
}
