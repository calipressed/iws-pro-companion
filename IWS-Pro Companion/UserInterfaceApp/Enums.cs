﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceApp
{
    public enum PageStates
    {
        Unknown,
        ServerRunning,
        ServerStopped,
        ServerStopping,
        ServerStarting
    }

    public enum Languages
    {
        De,
        En,
        Undefined
    }
}
