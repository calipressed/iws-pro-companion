﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UserInterfaceApp
{
    /// <summary>
    /// Interaktionslogik für CloseInfoPopup.xaml
    /// </summary>
    public partial class CloseInfoPopup : Window
    {
        #region constants

        #region ui texts
        private const string CheckboxTextEn = "Don't show this message again";
        private const string CheckboxTextDe = "Diese Nachricht nicht mehr anzeigen";

        private const string TextboxInfoTextEn = "The application is beeing minimized into the system Tray. If you want to close the app, use the 'Quit' Button instead";
        private const string TextboxInfoTextDe = "Die Applikation wird minimiert. Wenn Sie diese komplett schließen wollen, nutzen Sie bitte den 'Beenden'-Knopf";
        #endregion

        #endregion

        #region fields
        private Languages uiLanguage = Languages.Undefined;
        #endregion

        public CloseInfoPopup(Languages language)
        {
            this.InitializeComponent();

            // set fields
            this.uiLanguage = language;

            // set the displayed texts
            if (this.uiLanguage.Equals(Languages.De))
            {
                this.txbInfoText.Text = CloseInfoPopup.TextboxInfoTextDe;
                this.chbDoNotShowAgain.Content = CloseInfoPopup.CheckboxTextDe;
            }
            else
            {
                this.txbInfoText.Text = CloseInfoPopup.TextboxInfoTextEn;
                this.chbDoNotShowAgain.Content = CloseInfoPopup.CheckboxTextEn;
            }

            
        }

        #region event handlers
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            // set dialog result -> closes the dialog
            if (this.chbDoNotShowAgain.IsChecked == true)
                this.DialogResult = true;
            else
                this.DialogResult = false;
        }
        #endregion

        #region public static methods
        /// <summary>
        /// Shows the Close info popup window as a dialog window and lets the user choose wether they want to see this message again
        /// or switch it off
        /// </summary>
        /// <param name="language">current ui language selected</param>
        /// <returns>a boolean value containing wether this message should be showed again (false) or not (true)</returns>
        public static bool Show(Languages language)
        {
            bool result = false;

            // create the window
            CloseInfoPopup window = new CloseInfoPopup(language);

            // show dialog
            if (window.ShowDialog() == true)
                result = true;

            return result;
        }
        #endregion
    }
}
