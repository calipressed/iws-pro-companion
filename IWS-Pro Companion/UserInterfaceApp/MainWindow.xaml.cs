﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.ComponentModel;
using System.Net.Http;
using System.Configuration;
using System.IO;

namespace UserInterfaceApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region constants

        #region ui texts
        // texts
        private const string ButtonTextStartDe = "Start";
        private const string ButtonTextStopDe = "Stop";
        private const string ButtonTextQuitDe = "Beenden";

        private const string ButtonTextStartEn = "Start";
        private const string ButtonTextStopEn = "Stop";
        private const string ButtonTextQuitEn = "Quit";

        private const string TextboxTextStartingDe = "Server wird gestartet";
        private const string TextboxTextStoppingDe = "Server wird gestoppt";
        private const string TextboxTextStartedDe = "Server wird ausgeführt";
        private const string TextboxTextStoppedDe = "Server gestoppt";
        private const string TextboxTextSomethingWrongDe = "Etwas ist schief gelaufen!";

        private const string TextboxTextStartingEn = "server starting";
        private const string TextboxTextStoppingEn = "server stopping";
        private const string TextboxTextStartedEn = "server running";
        private const string TextboxTextStoppedEn = "server stopped";
        private const string TextboxTextSomethingWrongEn = "Something went wrong!";
        #endregion

        // configs
        private const string LanguageKey = "Language";
        private const string LanguageDeValue = "DE";
        private const string LanguageEnValue = "EN";
        private const string DontShowThisMessageAgainKey = "NoInfoPopup";
        private const string ShowServerWindowKey = "ShowServerWindow";
        private const string TrueValue = "TRUE";
        private const string FalseValue = "FALSE";

        private const string ServerExecutableName = "Server.exe";
        private const string ServerStartParameters = "-d";

        // routes
        private const string ServerStopRoute = "http://localhost:5055/F4E6618B7AA2";
        private const string ServerAppHealthcheckRoute = "http://localhost:5055/1DF1AB64";

        // wait times
        private const int WaitWhileServerStart = 1000;
        private const int WaitBetweenChecks = 4000;
        private const int WaitForShutdown = 2500;

        // general
        private const int MaxRetrys = 4;
        #endregion

        #region readonly fields
        private readonly Languages uiLanguage;
        #endregion

        #region fields
        // serverProcess to hold the info of the started process
        private Process serverProcess = null;

        // client for http requests
        private static readonly HttpClient client = new HttpClient();

        // notifyIcon -> icon in taskbar
        private System.Windows.Forms.NotifyIcon notifyIcon;

        // save the window state
        private WindowState storedWindowState = WindowState.Normal;

        // background workers for checks
        private BackgroundWorker checkServerStateWorker = null;
        private BackgroundWorker waitForServerStartWorker = null;

        // states
        private PageStates pageState = PageStates.Unknown;

        // retry counter
        private int retryCounter = -1;

        // booleans
        private bool closeApp = false; // app only closes if this was set
        #endregion

        #region constructor
        public MainWindow()
        {
            this.InitializeComponent();

            // read language from config and set it (cant move to method because uiLanguage is a readonly field)
            if (ConfigurationManager.AppSettings[MainWindow.LanguageKey].Equals(MainWindow.LanguageEnValue))
            {
                this.uiLanguage = Languages.En;
            }
            else if (ConfigurationManager.AppSettings[MainWindow.LanguageKey].Equals(MainWindow.LanguageDeValue))
            {
                this.uiLanguage = Languages.De;
            }
            else
            {
                this.uiLanguage = Languages.Undefined;
            }

            this.LanguageSelected();

            // init background workers
            this.waitForServerStartWorker = new BackgroundWorker();
            this.checkServerStateWorker = new BackgroundWorker();
            this.waitForServerStartWorker.DoWork += this.WaitForServerStartWorker_DoWork;
            this.checkServerStateWorker.DoWork += this.CheckServerStateWorker_DoWork;
            this.waitForServerStartWorker.RunWorkerCompleted += this.WaitForServerStartWorker_RunWorkerCompleted;
            this.checkServerStateWorker.RunWorkerCompleted += this.CheckServerStateWorker_RunWorkerCompleted;

            // setup notify icon
            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.Text = "IWS-Pro Companion App";
            notifyIcon.Icon = new System.Drawing.Icon("favicon.ico");
            notifyIcon.Click += new EventHandler(NotifyIcon_Click);

            // start the server
            this.StartServerAsCli();
            this.PageState = PageStates.ServerStarting;
        }
        #endregion

        #region properties
        private PageStates PageState
        {
            get { return this.pageState; }
            set
            {
                if (value.Equals(PageStates.Unknown))
                {
                    // this state should not be possible, but if this for some reason still happens
                    // -> only possibillity is to quit the application 
                    // change buttons so that only quit is an option
                    this.btnStart.IsEnabled = false;
                    this.btnStop.IsEnabled = false;
                    this.btnQuit.IsEnabled = true;

                    // change the running indicators
                    this.shpServerStateIndicator.Fill = Brushes.Gray;

                    if (this.uiLanguage.Equals(Languages.De))
                        this.txbServerStateIndicator.Text = MainWindow.TextboxTextSomethingWrongDe;
                    else
                        this.txbServerStateIndicator.Text = MainWindow.TextboxTextSomethingWrongEn;
                }
                else if (value.Equals(PageStates.ServerRunning) ||
                    value.Equals(PageStates.ServerStarting) ||
                    value.Equals(PageStates.ServerStopping))
                {
                    // change buttons
                    this.btnStart.IsEnabled = false;
                    this.btnStop.IsEnabled = true;
                    this.btnQuit.IsEnabled = false;

                    // change indicator and text
                    if (value.Equals(PageStates.ServerStarting) ||
                        value.Equals(PageStates.ServerStopping))
                    {
                        this.shpServerStateIndicator.Fill = Brushes.Orange;

                        if (value.Equals(PageStates.ServerStarting))
                        {
                            if (this.uiLanguage.Equals(Languages.De))
                                this.txbServerStateIndicator.Text = MainWindow.TextboxTextStartingDe;
                            else
                                this.txbServerStateIndicator.Text = MainWindow.TextboxTextStartingEn;
                        }
                        else
                        {
                            if (this.uiLanguage.Equals(Languages.De))
                                this.txbServerStateIndicator.Text = MainWindow.TextboxTextStoppingDe;
                            else
                                this.txbServerStateIndicator.Text = MainWindow.TextboxTextStoppingEn;
                        }
                    }
                    else
                    {
                        this.shpServerStateIndicator.Fill = Brushes.Green;

                        if (this.uiLanguage.Equals(Languages.De))
                            this.txbServerStateIndicator.Text = MainWindow.TextboxTextStartedDe;
                        else
                            this.txbServerStateIndicator.Text = MainWindow.TextboxTextStartedEn;
                    }
                }
                else if (value.Equals(PageStates.ServerStopped))
                {
                    // change buttons
                    this.btnStart.IsEnabled = true;
                    this.btnStop.IsEnabled = false;
                    this.btnQuit.IsEnabled = true;

                    this.shpServerStateIndicator.Fill = Brushes.Red;

                    if (this.uiLanguage.Equals(Languages.De))
                        this.txbServerStateIndicator.Text = MainWindow.TextboxTextStoppedDe;
                    else
                        this.txbServerStateIndicator.Text = MainWindow.TextboxTextStoppedEn;
                }

                this.pageState = value;
            }
        }
        #endregion

        #region event handlers

        #region window event handlers
        private void OnClose(object sender, CancelEventArgs args)
        {
            // this is beeing called every time the app is closed. Since we do want to overwrite the X Button, we use this method aswell.
            // we check if the closeApp boolean is set -> only set with the quit button 
            // and change the ui behaviour accordingly
            if (!closeApp)
            {
                // cancel close
                args.Cancel = true;

                // get folders and files
                string appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\IWS-Pro-Companion\\";
                string nopupupFile = appdataDir + "nopopup.conf";

                // create folder if not exists yet
                if (!Directory.Exists(appdataDir))
                {
                    Directory.CreateDirectory(appdataDir);
                }

                // check if user has turned off the info message
                if (!File.Exists(nopupupFile))
                {
                    if (CloseInfoPopup.Show(this.uiLanguage))
                    {
                        // check if file exists
                        if (!File.Exists(nopupupFile))
                        {
                            // create empty file
                            using (File.Create(nopupupFile)) { };
                        }
                    }
                }

                // minimize window instead
                this.WindowState = WindowState.Minimized;
            }
        }

        private void OnStateChanged(object sender, EventArgs args)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
            }
            else
            {
                this.storedWindowState = this.WindowState;
            }
        }

        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            CheckTrayIcon();
        }
        #endregion

        #region btn click events
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            // starts the server
            if (this.btnStart.IsEnabled)
            {
                // first check if the process is still running
                if (serverProcess == null || serverProcess.HasExited)
                {
                    // delete old process if it has exited but is not deleted yet
                    if (serverProcess != null)
                    {
                        serverProcess = null;
                    }

                    // start fresh with a new process
                    this.StartServerAsCli();
                }
            }
        }

        private async void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            if (this.btnStop.IsEnabled)
            {
                // check if server is running
                if ((serverProcess != null && !serverProcess.HasExited) || await CheckServerState())
                {
                    // change page state to stopping
                    PageState = PageStates.ServerStopping;

                    // send stop command and change the pagestate afterwards accordingly
                    if (await GracefullyShutdownServer())
                        PageState = PageStates.ServerStopped;
                    else
                        PageState = PageStates.Unknown;
                }
            }
        }

        private void BtnQuit_Click(object sender, RoutedEventArgs e)
        {
            if (this.btnQuit.IsEnabled)
            {
                // check if server is stopped
                if (this.serverProcess == null || this.serverProcess.HasExited)
                {
                    // clear server process 
                    if (this.serverProcess != null && this.serverProcess.HasExited)
                        this.serverProcess = null;

                    // quit app
                    this.QuitApp();
                }
            }
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            this.WindowState = this.storedWindowState;
        }
        #endregion

        #region background worker events

        #region do work
        private void WaitForServerStartWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (this.retryCounter.Equals(-1))
                this.retryCounter = 0;

            // sleep for a defined time
            System.Threading.Thread.Sleep(WaitWhileServerStart);
        }

        private void CheckServerStateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(WaitBetweenChecks);
        }
        #endregion

        #region worker completed
        private async void WaitForServerStartWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!await CheckServerState())
            {
                retryCounter++;

                // check if max amount of retrys was reached or if it can be checked again
                if (retryCounter <= MainWindow.MaxRetrys)
                {
                    waitForServerStartWorker.RunWorkerAsync();
                }
                else
                {
                    // reset retry counter
                    this.retryCounter = -1;

                    // change state to stopped again since it seems like the server wont start
                    this.PageState = PageStates.ServerStopped;
                }

            }
            else
            {
                // seems like the server is running
                this.PageState = PageStates.ServerRunning;

                // start online check worker
                if (!this.checkServerStateWorker.IsBusy)
                    this.checkServerStateWorker.RunWorkerAsync();
            }
        }

        private async void CheckServerStateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (await CheckServerState())
            {
                // check again
                this.checkServerStateWorker.RunWorkerAsync();
            }
            else
            {
                // remove process if it still exists and change the ui
                if (this.serverProcess != null)
                {
                    if (!this.serverProcess.HasExited)
                    {
                        this.serverProcess.Kill();
                    }

                    this.serverProcess = null;
                }

                this.PageState = PageStates.ServerStopped;
            }
        }
        #endregion

        #endregion

        #endregion

        #region private methods
        /// <summary>
        /// Checks the servers online state asyncronus and returns true if the server is running and false if it is not
        /// </summary>
        /// <returns>true is the server is running</returns>
        private async Task<bool> CheckServerState()
        {
            bool result = false;

            // try to make a get request to the healthcheck route
            try
            {
                await client.GetStringAsync(MainWindow.ServerAppHealthcheckRoute);

                // we do not care about the outcome -> if we reached this point, the server did respond
                result = true;
            }
            catch (Exception e)
            {
                // do nothing
            }

            return result;
        }

        /// <summary>
        /// changes the text displayed on the buttons depending on the selected language
        /// </summary>
        private void LanguageSelected()
        {
            if (this.uiLanguage.Equals(Languages.De))
            {
                this.btnStart.Content = MainWindow.ButtonTextStartDe;
                this.btnStop.Content = MainWindow.ButtonTextStopDe;
                this.btnQuit.Content = MainWindow.ButtonTextQuitDe;
            }
            else
            {
                this.btnStart.Content = MainWindow.ButtonTextStartEn;
                this.btnStop.Content = MainWindow.ButtonTextStopEn;
                this.btnQuit.Content = MainWindow.ButtonTextQuitEn;
            }
        }

        private async Task<bool> GracefullyShutdownServer()
        {
            bool result = false;

            // try to make a get request to the healthcheck route
            try
            {
                await client.GetStringAsync(MainWindow.ServerStopRoute);

                // we do not care about the outcome -> if we reached this point, the server did respond

                // give server time for stop
                System.Threading.Thread.Sleep(MainWindow.WaitForShutdown);

                if (serverProcess.HasExited)
                {
                    result = true;
                }
            }
            catch (Exception e)
            {
                // do nothing
            }

            return result;
        }

        private void CheckTrayIcon()
        {
            ShowTrayIcon(!IsVisible);
        }

        private void ShowTrayIcon(bool show)
        {
            if (notifyIcon != null)
                notifyIcon.Visible = show;
        }

        /// <summary>
        /// Launch the application with some options set.
        /// </summary>
        private void StartServerAsCli()
        {
            // change pagestate
            this.PageState = PageStates.ServerStarting;

            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = false;
            startInfo.FileName = MainWindow.ServerExecutableName;

            string args = MainWindow.ServerStartParameters;

            // var for the tmp files
            string tempPath = "";

            // check if server folder was provided in the app settings
            if (ConfigurationManager.AppSettings["ServerTmpFolder"] != "")
            {
                tempPath = ConfigurationManager.AppSettings["ServerTmpFolder"];
                args += " --folder=" + tempPath;
            }
            else
            {
                // get path from env var
                tempPath = Environment.GetEnvironmentVariable("TEMP");

                // check if env var was filled
                if (!string.IsNullOrEmpty(tempPath))
                {
                    // add subfolder
                    tempPath += "\\IWS-Pro-Companion\\";
                }
                else
                {
                    // check for the tmp var
                    tempPath = Environment.GetEnvironmentVariable("TMP");

                    if (!string.IsNullOrEmpty(tempPath))
                    {
                        // add subfolder
                        tempPath += "\\IWS-Pro-Companion\\";
                    }
                    else
                    {
                        MessageBox.Show("No valid temp folder found. Please add your own in the App.config file");
                        App.Current.Shutdown();
                    }
                }
            }

            // create temp dir if needed
            if (!string.IsNullOrEmpty(tempPath) && !Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }

            string appdataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\IWS-Pro-Companion\\";

            // check and create appdata dir
            if (!Directory.Exists(appdataDir))
            {
                Directory.CreateDirectory(appdataDir);
            }

            startInfo.Arguments = args;

            // check if window should be created
            if (ConfigurationManager.AppSettings[MainWindow.ShowServerWindowKey].Equals(MainWindow.FalseValue))
            {
                startInfo.CreateNoWindow = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            }
            else
            {
                startInfo.CreateNoWindow = false;
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
            }

            try
            {
                // Start the process with the info we specified.
                serverProcess = Process.Start(startInfo);

                // start the first check as a background process
                waitForServerStartWorker.RunWorkerAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // Log error.
            }
        }

        /// <summary>
        /// Quits the application, disposes of all elements and sets the close variable
        /// </summary>
        private void QuitApp()
        {
            // clear notify icon
            notifyIcon.Dispose();
            notifyIcon = null;

            // set close app -> closes in event handler
            this.closeApp = true;

            // call close and event handler
            this.Close();
        }
        #endregion
    }
}
