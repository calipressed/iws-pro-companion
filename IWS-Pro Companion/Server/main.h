#pragma once

#ifndef main_h
#define main_h

#include <stdio.h>

#include <string>
class Webserver {
public:
    static void startServer(bool debugFlag, bool nFlag, bool oFlag, bool pFlag, bool lFlag, bool writeFile, bool doCertificateValidation);
};

#endif /* main_h */